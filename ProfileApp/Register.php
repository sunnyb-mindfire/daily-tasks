<?php
session_start();
require_once('DBConnection.php');
require_once('Constants.php');

$errors = array();
if (isset($_POST['submit']) && $_POST['submit']) {
    require_once('Methods.php');
    require_once('Validate.php');

    if (!empty($errors)) {
        $_SESSION['errors'] = $errors;
        $_SESSION['oldValues'] = $_POST;
        header("location:Registration.php");
    } else {
        //photo upload
        if (isset($fileExt) && isset($fileTempName)) {
            $imageName = rand() . '_' . time() . '_' . rand() . '.' . $fileExt;
            move_uploaded_file($fileTempName, PROFILE_IMAGE_PATH.$imageName);
            $photoLocation = $imageName;
        } else {
            $photoLocation = '';
        }

        try {
            $con->beginTransaction();
            //if company exists
            if ($PK_ID = ifExistsInDB('company', 'name', $company, $con)) {
                $FK_companyID = $PK_ID;
            } else {
                //inserting data into company table
                $data = array();
                $data['name'] = $company;
                $FK_companyID = insertIntoDB('company', $data, $con);
            }

            //if employment exists
            if ($PK_ID = ifExistsInDB('employment', 'role', $employment, $con)) {
                $FK_employmentID = $PK_ID;
            } else {
                //inserting data into employment table
                $data = array();
                $data['role'] = $employment;
                $FK_employmentID = insertIntoDB('employment', $data, $con);
            }

            //inserting data into employee table
            $data = array();
            $data['prefix'] = $prefix;
            $data['firstName'] = $firstName;
            $data['middleName'] = $middleName;
            $data['lastName'] = $lastName;
            $data['emailID'] = $email;
            $data['password'] = $password;
            $data['gender'] = $gender;
            $data['dateOfBirth'] = $dateOfBirth;
            $data['maritalStatus'] = $maritalStatus;
            $data['FK_companyID'] = $FK_companyID;
            $data['FK_employmentID'] = $FK_employmentID;
            $data['photoLocation'] = $photoLocation;
            $data['fax'] = $fax;
            $data['extraNote'] = $extraNote;
            $FK_employeeID = insertIntoDB('employee', $data, $con);

            //inserting residence address
            if (isset($residenceState)) {
                $addressData = array(
                    'addressType' => 'RESIDENCE',
                    'state' => $residenceState,
                    'city' => $residenceCity,
                    'street' => $residenceStreet,
                    'zipCode' => $residenceZipCode,
                    'FK_employeeID' => $FK_employeeID
                    );
                insertAddressIntoDB($addressData, $con);
            }

            //inserting office address
            if (isset($officeState)) {
                $addressData = array(
                    'addressType' => 'OFFICE',
                    'state' => $officeState,
                    'city' => $officeCity,
                    'street' => $officeStreet,
                    'zipCode' => $officeZipCode,
                    'FK_employeeID' => $FK_employeeID
                    );
                insertAddressIntoDB($addressData, $con);
            }

            //inserting data into contact table
            if (isset($homePhone)) {
                $numberType = 'HOME';
                $data = array();
                $data['number'] = $homePhone;
                $data['type'] = $numberType;
                $data['FK_employeeID'] = $FK_employeeID;
                $contactPK_ID = insertIntoDB('contact', $data, $con);
            }
            if (isset($cellPhone)) {
                $numberType = 'MOBILE';
                $data = array();
                $data['number'] = $cellPhone;
                $data['type'] = $numberType;
                $data['FK_employeeID'] = $FK_employeeID;
                $contactPK_ID = insertIntoDB('contact', $data, $con);
            }

            //inserting into communication medium and employee communication
            insertCommunicationIntoDB($communicationMedium, $FK_employeeID, $con);

            $con->commit();
        } catch (PDOException $ex) {
            $con->rollBack();
            logError($ex->getMessage());
            $_SESSION['dbError'] = 1;
            header('location:Login.php');
        }

        echo 'Registration Successful!You are being redirected to login......';
        header("refresh:3;url=Login.php");
    }
} else {
    header("location:Login.php");
}
