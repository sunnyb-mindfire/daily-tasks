<?php
require_once('Constants.php');
require_once(CONFIG_PATH . 'Config.php');
require_once('Methods.php');

try {
    $con = new PDO($dsn, $userName, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $ex) {
    logError($ex->getMessage());
    $_SESSION['dbError'] = 1;
}
