<?php
session_start();
if (!isset($_SESSION['loginStatus']) ||  1 != $_SESSION['loginStatus']) {
    header("location:Login.php");
}
$_SESSION = array();
session_destroy();
header("location:Login.php");
