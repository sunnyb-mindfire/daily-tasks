<?php
session_start();
if (!isset($_SESSION['loginStatus']) ||  1 != $_SESSION['loginStatus']) {
    header("location:Login.php");
} else {
    require_once('DBConnection.php');

    $userSql = "SELECT prefix, firstName, middleName, lastName
        FROM employee
        WHERE emailID = :emailID";

    try {
        $userStmt = $con->prepare($userSql);
        $userStmt->bindParam(':emailID', $_SESSION['emailID']);
        $userStmt->execute();
        $userData = $userStmt->fetch(PDO::FETCH_OBJ);
    } catch (PDOException $ex) {
        echo $ex->getMessage();
        exit();
    }

    $sql = "SELECT
        emp.PK_ID,
        CONCAT_WS(
            ' ',
            emp.prefix,
            emp.firstName,
            emp.middleName,
            emp.lastName
          )
        AS fullName,
        emp.emailID,
        emp.gender,
        comp.name AS companyName,
        employment.role AS designation,
        GROUP_CONCAT(
            CONCAT_WS(
                ', ',
                addr.type,
                addr.street,
                city.name,
                state.name,
                addr.zipCode
            )
            SEPARATOR ', '
        )
        AS fullAddress,
        GROUP_CONCAT(
            CONCAT_WS(
                ' : ',
                contact.type,
                contact.number
            )
            SEPARATOR ', '
        )
        AS contactInfo,
        GROUP_CONCAT(
            communication.medium
            SEPARATOR ', '
        )
        AS prefCommMedium
    FROM
        employee emp
    INNER JOIN
        company comp ON emp.FK_companyID = comp.PK_ID
    INNER JOIN
        employment ON emp.FK_employmentID = employment.PK_ID
    INNER JOIN
        address addr ON emp.PK_ID = addr.FK_employeeID
    INNER JOIN
        city ON addr.FK_cityID = city.PK_ID
    INNER JOIN
        state ON city.FK_stateID = state.PK_ID
    INNER JOIN
        contact ON emp.PK_ID = contact.FK_employeeID
    INNER JOIN
        employeeCommunication empComm ON emp.PK_ID = empComm.FK_employeeID
    INNER JOIN
        communication ON empComm.FK_communicationID = communication.PK_ID
    GROUP BY emp.emailID";

    try {
        $stmt = $con->prepare($sql);
        $stmt->execute();
    } catch (PDOException $ex) {
        logError($ex->getMessage());
        $_SESSION['dbError'] = 1;
        header('location:Login.php');
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
        rel="stylesheet" type="text/css">
        <link href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"
        rel="stylesheet" type="text/css">        
    </head>
    
    <body>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#collapsabledNavBar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <i class="fa fa-tachometer fa-3x fa-fw" aria-hidden="true"></i>Dashboard
                </div>
                <div class="collapse navbar-collapse" id="collapsabledNavBar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active">
                            <a href="Dashboard.php">
                                Dashboard
                                <i class="fa fa-star fa-fw"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                <i class="fa fa-user fa-lg fa-fw"></i>
                                <?php echo $userData->firstName; ?>
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="Logout.php">Log Out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center text-muted">
                            Welcome
                            <?php
                            $fullName = $userData->prefix . ' ' . $userData->firstName . ' '
                                . $userData->middleName . ' ' . $userData->lastName;
                            echo $fullName;
                            ?>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 table-responsive">
                        <table class="table" id="userDetailsTable">
                            <thead>
                                <tr>
                                    <th rowspan="2">#</th>
                                    <th rowspan="2">Name</th>
                                    <th rowspan="2">Email</th>
                                    <th rowspan="2">Gender</th>
                                    <th rowspan="2">Company</th>
                                    <th rowspan="2">Designation</th>
                                    <th rowspan="2">Address</th>
                                    <th rowspan="2">Contact No.</th>
                                    <th rowspan="2">Preffered Comm. Medium</th>
                                    <th colspan="3">Operations</th>
                                </tr>
                                <tr>                                    
                                    <th style="display: none;"></th>
                                    <th style="display: none;"></th>
                                    <th style="display: none;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $slNo = 1;
                            while ($data = $stmt->fetch(PDO::FETCH_OBJ)) {
                                $fullAddress = implode(', ', array_unique(explode(', ', $data->fullAddress)));
                                $contactInfo = implode(', ', array_unique(explode(', ', $data->contactInfo)));
                                $prefCommMedium = implode(', ', array_unique(explode(', ', $data->prefCommMedium)));
                            ?>
                                <tr id="<?php echo $data->PK_ID; ?>">
                                    <td><?php echo $slNo++; ?></td>
                                    <td><?php echo $data->fullName; ?></td>
                                    <td><?php echo $data->emailID; ?></td>
                                    <td><?php echo $data->gender; ?></td>
                                    <td><?php echo $data->companyName; ?></td>
                                    <td><?php echo $data->designation; ?></td>
                                    <td><?php echo $fullAddress; ?></td>
                                    <td><?php echo $contactInfo; ?></td>
                                    <td><?php echo $prefCommMedium; ?></td>
                                    <td>
                                        <i class="fa fa-eye fa-lg text-info" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-lg fa-pencil text-success" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <i class="fa fa-lg fa-trash text-danger" aria-hidden="true"></i>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript"
        src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js">
        </script>
        <script type="text/javascript"
        src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js">
        </script>
        <script type="text/javascript"
        src="http://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#userDetailsTable').DataTable();
            });
        </script>
    </body>
</html>