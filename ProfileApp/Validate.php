<?php
require_once('Methods.php');

//prefix field validation
$message = requiredFieldValidate($_POST['prefix'], 'Prefix');
if (!is_string($message) || !$errors['prefix'] = $message) {
    $prefix = trim($_POST['prefix']);
}

//firstName field validation
$message = requiredFieldValidate($_POST['firstName'], 'First name', true);
if (!is_string($message) || !$errors['firstName'] = $message) {
    $firstName = trim($_POST['firstName']);
}

//middleName field validation
if (isset($_POST['middleName'])) {
    $middleName = trim($_POST['middleName']);
}

//lastName field validation
$message = requiredFieldValidate($_POST['lastName'], 'Last name', true);
if (!is_string($message) || !$errors['lastName'] = $message) {
    $lastName = trim($_POST['lastName']);
}

//email field validation
$message = requiredFieldValidate($_POST['email'], 'Email');
if (!is_string($message) || !$errors['email'] = $message) {
    if (!filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = 'Invalid email ID format.';
    } else {
        $email = trim($_POST['email']);
    }
}

//if email ID exists
if (isset($email) && ifExistsInDB('employee', 'emailID', $email, $con)) {
    $errors['email'] = 'Email ID already exists.';
}

//password field validataion
$message = requiredFieldValidate($_POST['password'], 'Password');
if (!is_string($message) || !$errors['password'] = $message) {
    if (strlen(trim($_POST['password'])) < 8 || strlen(trim($_POST['password'])) > 15) {
        $errors['password'] = 'Password must be 8 to 15 characters long.';
    } else {
        $password = $_POST['password'];
    }
}

//confirmPassword field validation
$message = requiredFieldValidate($_POST['confirmPassword'], 'Confirm password');
if (isset($password) && (!is_string($message) || !$errors['confirmPassword'] = $message)) {
    if ($password != trim($_POST['confirmPassword'])) {
        $errors['confirmPassword'] = 'Confirm password mismatch.';
    } else {
        $confirmPassword = $_POST['confirmPassword'];
    }
}

//gender field validation
$message = requiredFieldValidate($_POST['gender'], 'Gender');
if (!is_string($message) || !$errors['gender'] = $message) {
    $gender = $_POST['gender'];
}

//dateOfBirth field validation
$message = requiredFieldValidate($_POST['dateOfBirth'], 'Date of birth');
if (!is_string($message) || !$errors['dateOfBirth'] = $message) {
    if (validateDateFormat(trim($_POST['dateOfBirth'])) && validateAge(trim($_POST['dateOfBirth']))) {
        $dateOfBirth = trim($_POST['dateOfBirth']);
    } else {
        $errors['dateOfBirth'] = 'Invalid date format or your age is less than 18 years.';
    }
}

//maritalStatus field validation
$message = requiredFieldValidate($_POST['maritalStatus'], 'Marital status');
if (!is_string($message) || !$errors['maritalStatus'] = $message) {
    $maritalStatus = trim($_POST['maritalStatus']);
}

//company field validation
$message = requiredFieldValidate($_POST['company'], 'Company');
if (!is_string($message) || !$errors['company'] = $message) {
    $company = trim($_POST['company']);
}

//employment field validation
$message = requiredFieldValidate($_POST['employment'], 'Employment');
if (!is_string($message) || !$errors['employment'] = $message) {
    $employment = trim($_POST['employment']);
}

//photo validation
if (isset($_FILES['photo']['name']) &&  !empty($_FILES['photo']['name'])) {
    $fileName = $_FILES['photo']['name'];
    $fileSize = $_FILES['photo']['size'];
    $fileType = mime_content_type($_FILES['photo']['tmp_name']);
    $fileExt = strtolower(end(explode('.', $_FILES['photo']['name'])));
    $fileTempName = $_FILES['photo']['tmp_name'];
    $allowedFileTypes = array (
                        'image/gif',
                        'image/png',
                        'image/jpeg',
                        'image/JPEG',
                        'image/PNG',
                        'image/GIF'
                        );
    if (!in_array($fileType, $allowedFileTypes)) {
        $errors['photo'] = 'File type is not allowed.';
    }
}

//residence address validation
if (isset($_POST['residenceStreet']) && !empty(trim($_POST['residenceStreet']))) {
    $residenceStreet = trim($_POST['residenceStreet']);
}
if (isset($_POST['residenceCity']) && !empty(trim($_POST['residenceCity']))) {
    $message = isOnlyLetter($_POST['residenceCity'], 'Residence city');
    if (!is_string($message) || !$errors['residenceCity'] = $message) {
        $residenceCity = trim($_POST['residenceCity']);
    }
}
if (isset($_POST['residenceState']) && !empty(trim($_POST['residenceState']))) {
    $message = isOnlyLetter($_POST['residenceState'], 'Residence state');
    if (!is_string($message) || !$errors['residenceState'] = $message) {
        $residenceState = trim($_POST['residenceState']);
    }
}
if (isset($_POST['residenceZipCode']) && !empty(trim($_POST['residenceZipCode']))) {
    if (!ctype_digit(trim($_POST['residenceZipCode'])) || strlen(trim($_POST['residenceZipCode'])) != 6) {
        $errors['residenceZipCode'] = 'Invalid ZipCode.';
    } else {
        $residenceZipCode = trim($_POST['residenceZipCode']);
    }
}

//office address validation
if (isset($_POST['officeStreet']) && !empty(trim($_POST['officeStreet']))) {
    $officeStreet = trim($_POST['officeStreet']);
}
if (isset($_POST['officeCity']) && !empty(trim($_POST['officeCity']))) {
    $message = isOnlyLetter($_POST['officeCity'], 'Office city');
    if (!is_string($message) || !$errors['officeCity'] = $message) {
        $officeCity = trim($_POST['officeCity']);
    }
}
if (isset($_POST['officeState']) && !empty(trim($_POST['officeState']))) {
    $message = isOnlyLetter($_POST['officeState'], 'Office state');
    if (!is_string($message) || !$errors['officeState'] = $message) {
        $officeState = trim($_POST['officeState']);
    }
}
if ((isset($_POST['officeZipCode']) && !empty(trim($_POST['officeZipCode'])))) {
    if (!ctype_digit(trim($_POST['officeZipCode'])) || strlen(trim($_POST['officeZipCode'])) != 6) {
        $errors['officeZipCode'] = 'Invalid ZipCode.';
    } else {
        $officeZipCode = trim($_POST['officeZipCode']);
    }
}

//home and cell phone number validation
if ((isset($_POST['homePhone']) && !empty(trim($_POST['homePhone'])))
    || (isset($_POST['cellPhone']) && !empty(trim($_POST['cellPhone'])))) {
    if (isset($_POST['homePhone']) && !empty(trim($_POST['homePhone']))) {
        if (!ctype_digit(trim($_POST['homePhone'])) && strlen(trim($_POST['homePhone'])) != 10) {
            $errors['homePhone'] = 'Invalid home phone number.';
        } else {
            $homePhone = trim($_POST['homePhone']);
        }
    }

    if (isset($_POST['cellPhone']) && !empty(trim($_POST['cellPhone']))) {
        if (!ctype_digit(trim($_POST['cellPhone'])) && strlen(trim($_POST['homePhone'])) != 10) {
            $errors['cellPhone'] = 'Invalid cell phone number.';
        } else {
            $cellPhone = trim($_POST['cellPhone']);
        }
    }
} else {
    $errors['phoneNumber'] = 'Please provide either home or cell phone number.';
}

//communication medium field validation
if (isset($_POST['communicationMedium'])) {
    $communicationMedium = $_POST['communicationMedium'];
} else {
    $errors['communicationMedium'] = 'Please select atleast one communication medium.';
}

//fax field validation
if (isset($_POST['fax']) && ctype_digit(trim($_POST['cellPhone']))) {
    $fax = trim($_POST['fax']);
} else {
    $errors['fax'] = 'Invalid fax number.';
}

//extra note field validation
if (isset($_POST['extraNote'])) {
    $extraNote = $_POST['extraNote'];
}
