<?php
session_start();
if (isset($_SESSION['loginStatus']) &&  1 == $_SESSION['loginStatus']) {
    header("location:Dashboard.php");
}

require_once('DBConnection.php');

if (isset($_POST['submit']) && $_POST['submit']) {
    $errors = array();

    //validation of email field
    if (isset($_POST['email']) && empty(trim($_POST['email']))) {
        $errors['email'] = 'Email ID cannot be empty.';
    } else {
        $email = $_POST['email'];
    }

    //validation of password field
    if (isset($_POST['password']) && empty(trim($_POST['password']))) {
        $errors['password'] = 'Password cannot be empty.';
    } else {
        $password = $_POST['password'];
    }
    if (isset($email) && isset($password)) {
        //fetch data to check login
        $sql = "SELECT PK_ID FROM employee WHERE emailID =  :emailID AND password = :password";
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindParam(':emailID', $email);
            $stmt->bindParam(':password', $password);
            $stmt->execute();
        } catch (PDOException $ex) {
            logError($ex->getMessage());
            $_SESSION['dbError'] = 1;
        }

        //if email ID is correct
        if ($stmt->rowCount() > 0) {
            $_SESSION['loginStatus'] = 1;
            $_SESSION['emailID'] = $email;
            header("location:Dashboard.php");
        } else {
            $errors['login'] = 'Invalid Credentials.';
        }
    }
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
        rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div class="hidden-sm hidden-xs section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"></div>
                </div>
            </div>
        </div>
        <div class="hidden-xs section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"></div>
                </div>
            </div>
        </div>
        <?php
        if (isset($_SESSION['dbError']) && 1 == $_SESSION['dbError']) {
            echo '<div class="alert alert-danger text-center" style="width:50%; margin: 2% auto;">
                    <strong>Error: Oops!</strong> There was an unexpected error. Please try again later.
                </div>';
        }
        ?>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Enter Login Details</h3>
                            </div>
                            <div class="text-center">
                                <?php
                                if (isset($errors['login'])) {
                                    echo '<h5 class="text-danger">' . $errors['login'] . '</h5>';
                                }
                                ?>
                            </div>
                            <div class="panel-body">
                                <form method="POST" action="">
                                    <div class="form-group">
                                        <label class="control-label" for="email">Email ID</label>
                                        <input name="email" type="email" required class="form-control"
                                        id="email" placeholder="example@example.com">
                                        <?php
                                        if (isset($errors['email'])) {
                                            echo '<span class="text-danger">' . $errors['email'] . '</span>';
                                        }
                                        ?>
                                        <span id="emailErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="password">Password</label>
                                        <input name="password" type="password" required
                                        class="form-control" id="password" placeholder="****">
                                        <?php
                                        if (isset($errors['password'])) {
                                            echo '<span class="text-danger">' . $errors['password'] . '</span>';
                                        }
                                        ?>
                                        <span id="passwordErr" class="text-danger"></span>
                                    </div>
                                    <input type="submit" class="btn btn-default" name="submit"
                                    value="Login" id="submit">
                                </form>
                            </div>
                        </div>
                        <div class="text-center col-md-12">
                            <div class="col-md-6">
                                <a href="">Forgot password?</a>
                            </div>
                             <div class="col-md-6">
                                <a href="Registration.php">Not registered yet?</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js">
        </script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js">
        </script>
        <script type="text/javascript"src="LoginValidationScript.js">
        </script>
    </body>
</html>
<?php
unset($_SESSION['dbError']);
?>