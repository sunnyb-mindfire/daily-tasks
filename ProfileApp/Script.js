var result = true;

$(document).ready(function () {
    //declare regular expression
    var onlyLetterRegExp = /^[a-z\s]*$/i;
    var emailRegExp = /(\w+)\@(\w+)\.(\w+)/g;
    var onlyNumberRegExp = /^[0-9]*$/i;

    //on click on submit checking all fields empty or not
    $("#submit").click(function () {
        result = true;

        //validate each required field
        $("input:required").each(function () {
            isNotEmpty(this, getErrorFieldObj(this));
        });

        //validate gender
        eitherOrChecked($("#genderErr"), "gender", $("#genderMale"), $("#genderFemale"));

        //validate communication medium
        eitherOrChecked($("#communicationMediumErr"), "communication medium", $("#mail"), $("#message"), $("#call"));

        //one of the phone number is mandatory
        eitherOrEmpty($("#phoneNumberErr"), $("#homePhone"), $("#cellPhone"));

        //date of birth validation
        isDateOfBirthValid($("#dateOfBirth"), getErrorFieldObj($("#dateOfBirth")));

        return result;
    });

    //only letter field validation
    $("input.onlyLetter").keyup(function () {
        isValid(this, getErrorFieldObj(this), onlyLetterRegExp);
        return result;
    });

    //only number field validation
    $("input.onlyNumber").keyup(function () {
        isValid(this, getErrorFieldObj(this), onlyNumberRegExp);
        return result;
    });

    //email validation
    $("#email").keyup(function () {
        isValid(this, getErrorFieldObj(this), emailRegExp);
        return result;
    });

    //password validation
    $("#password").keyup(function () {
        isPasswordValid(this, getErrorFieldObj(this));
        return result;
    });

    //confirm password match validation
    $("#confirmPassword").keyup(function () {
        isEqual(this, $("#password"), getErrorFieldObj(this));
        return result;
    });

    //field with maxium length validation
    $("input[max]").blur(function () {
        isExactLength(this, getErrorFieldObj(this), parseInt($(this).attr("max")));
        return result;
    });
});

//method to get object of error message field corrospoiding to each field
function getErrorFieldObj(fieldObj)
{
    var errFieldId = "#" + $(fieldObj).attr("id") + "Err";
    return $(errFieldId);
}

//checking match with exact regular expression 
function isValid(fieldObj, errFieldObj, regExp)
{
    if (regExp.test($.trim($(fieldObj).val()))) {
        $(errFieldObj).html("");
        return true;
    } else {
        $(errFieldObj).html("Invalid format.");
        result = false;
        return false;
    }
}

//exact length matching
function isExactLength(fieldObj, errFieldObj, limit)
{
    var len = $.trim($(fieldObj).val()).length;
    if (0 !== len && len !== limit) {
        $(errFieldObj).html("This field should be " + limit + " digits.");
        result = false;
        return false;
    } else {
        return true;
    }
}

//password validation
function isPasswordValid(fieldObj, errFieldObj)
{
    var len = $.trim($(fieldObj).val()).length;
    if (len < 8 || len > 15) {
        $(errFieldObj).html("Password must be 8 to 15 characters long.");
        result = false;
        return false;
    } else {
        $(errFieldObj).html("");
        return true;
    }
}

//checking values of two field objects equal or not
function isEqual(fieldObj1, fieldObj2, errFieldObj)
{
    var value1 = $.trim($(fieldObj1).val());
    var value2 = $.trim($(fieldObj2).val());
    if (value1 !== value2) {
        $(errFieldObj).html("Confirm password mismatch.");
        result = false;
        return false;
    } else {
        $(errFieldObj).html("");
        return true;
    }
}

//checking any of the specified field is empty or not
function eitherOrEmpty(fieldObj, ...args)
{
    for (var i = 0; i < args.length; i++) {
        if ('' !== $(args[i]).val()) {
            $(fieldObj).html("");
            return true;
        }
    }
    $(fieldObj).html("Please provide either home or cell phone number.");
    result = false;
    return false;
}

//checking any one of the specified field is selected or not
function eitherOrChecked(fieldObj, msg, ...args)
{
    for (var i = 0; i < args.length; i++) {
        if ($(args[i]).prop("checked")) {
            $(fieldObj).html("");
            return true;
        }
    }
    $(fieldObj).html("Please select a " + msg + ".");
    result = false;
    return false;
}

//date validation
function isDateOfBirthValid(fieldObj, errFieldObj)
{
    //checking for date format
    var dateRegExp = /(\d\d\d\d)+\-(\d\d)+\-(\d\d)+/g;
    if (!isValid(fieldObj, errFieldObj, dateRegExp)) {
        result = false;
        return false;
    }

    var dateValue = $.trim($(fieldObj).val());

    var dateArray = dateValue.split('-');

    var day = parseInt(dateArray[2]);
    var month = parseInt(dateArray[1]);
    var year = parseInt(dateArray[0]);

    if (isDateValid(day, month, year)) {
        if (isAgeValid(day, month, year)) {
            $(errFieldObj).html("");
            result = true;
            return result;
        } else {
            $(errFieldObj).html("Your age is less than 18 years.");
            result = false;
            return result;
        }
    } else {
        $(errFieldObj).html("Invalid date.");
        result = false;
        return result;
    }
}

//checking wheither date and month is valid or not
function isDateValid(day, month, year)
{
    if (day < 1 || day > 31) {
        return false;
    }
    if (month < 1 || month > 12) {
        return false;
    }

    var dayInMonth = 31;

    switch (month) {
        case 4:
        case 6:
        case 9:
        case 11:
            dayInMonth = 30;
            break;

        default:
            break;
    }

    if (30 === dayInMonth && 31 === day) {
        return false;
    }

    if (month === 2) {
        var isLeap = (year % 4 === 0 && (year % 100 !== 0 || year % 400 === 0));
        if (day > 29 || (day === 29 && !isLeap)) {
            return false;
        }
    }
    return true;
}

function isAgeValid(day, month, year)
{
    //checking for 18 years old
    var age = 18;
    var dob = new Date();
    dob.setFullYear(year, month - 1, day);

    var currentDate = new Date();
    var setDate = new Date();
    setDate.setFullYear(dob.getFullYear() + age, month - 1, day);

    return ((currentDate - setDate) > 0);
}

//field is empty or not
function isNotEmpty(fieldObj, errFieldObj)
{
    if (0 === $.trim($(fieldObj).val()).length) {
        $(errFieldObj).html("You can\'t leave this empty.");
        result = false;
        return false;
    } else {
        $(errFieldObj).html("");
        return true;
    }
}