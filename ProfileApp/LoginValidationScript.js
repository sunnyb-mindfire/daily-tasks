var result = true;

$(document).ready(function () {
    //declare regular expression
    var emailRegExp = /(\w+)\@(\w+)\.(\w+)/g;

    $("#submit").click(function () {
        result = true;

        //validate each required field
        $("input:required").each(function () {
            isNotEmpty(this, getErrorFieldObj(this));
        });
        return result;
    });

    //email validation
    $("#email").keyup(function () {
        isValid(this, getErrorFieldObj(this), emailRegExp);
        return result;
    });
});

//method to get object of error message field corrospoiding to each field
function getErrorFieldObj(fieldObj)
{
    var errFieldId = "#" + $(fieldObj).attr("id") + "Err";
    return $(errFieldId);
}

//checking match with exact regular expression 
function isValid(fieldObj, errFieldObj, regExp)
{
    if (regExp.test($.trim($(fieldObj).val()))) {
        $(errFieldObj).html("");
        return true;
    } else {
        $(errFieldObj).html("Invalid input.");
        result = false;
        return false;
    }
}

//field is empty or not
function isNotEmpty(fieldObj, errFieldObj)
{
    if (0 === $.trim($(fieldObj).val()).length) {
        $(errFieldObj).html("This field cannot be empty.");
        result = false;
        return false;
    } else {
        $(errFieldObj).html("");
        return true;
    }
}