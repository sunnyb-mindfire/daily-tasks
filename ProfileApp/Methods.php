<?php
require_once('Constants.php');

//required field validation method
function requiredFieldValidate($fieldValue, $message, $isOnlyLetter = false)
{
    if (isset($fieldValue) && !empty(trim($fieldValue))) {
        if ($isOnlyLetter) {
            return isOnlyLetter($fieldValue, $message);
        }
        return true;
    } else {
        return $message . ' cannot be empty.';
    }
}

//validation method for only letter fields
function isOnlyLetter($fieldValue, $message)
{
    if (preg_match('/^[A-Z\s]*$/i', trim($fieldValue))) {
        return true;
    } else {
        return 'Please use only letters (a-z) for ' . $message;
    }
}

//date format validation
function validateDateFormat($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

//18 years old validation
function validateAge($date)
{
    $today = new DateTime('now');
    $dateBefore18Years = $today->modify('-18 year')->format('Y-m-d');
    return strtotime($dateBefore18Years) >= strtotime($date);
}

//if exists in database returns PK_ID
function ifExistsInDB($table, $attribute, $value, $con)
{
    try {
        $sql = "SELECT PK_ID FROM $table WHERE $attribute = :value";
        $stmt = $con->prepare($sql);
        $stmt->bindParam(':value', $value);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $data = $stmt->fetch(PDO::FETCH_OBJ);
            return $data->PK_ID;
        } else {
            return false;
        }
    } catch (PDOException $ex) {
        echo $ex->getMessage();
        exit();
    }
}

//method to insert data into database and returns lastInsertID
function insertIntoDB($table, $dataArray, $con)
{
    $sql = "INSERT INTO $table SET ";
    foreach ($dataArray as $attribute => $value) {
        $sql .= $attribute . ' = :' . $attribute . ', ';
    }
    $sql = substr($sql, 0, -2);
    try {
        $stmt = $con->prepare($sql);
        foreach ($dataArray as $attribute => $value) {
            $stmt->bindValue($attribute, $value);
        }
        $stmt->execute();
        return $con->lastInsertId();
    } catch (PDOException $ex) {
        echo $ex->getMessage();
        exit();
    }
}

//method to insert address
function insertAddressIntoDB($addressData, $con)
{
    $addressType = $addressData['addressType'];

    //if residence state exists
    if ($PK_ID = ifExistsInDB('state', 'name', $addressData['state'], $con)) {
        $FK_stateID = $PK_ID;
    } else {
        //inserting data into state table
        $data = array();
        $data['name'] = $addressData['state'];
        $FK_stateID = insertIntoDB('state', $data, $con);
    }

    //if residence city exists
    if ($PK_ID = ifExistsInDB('city', 'name', $addressData['city'], $con)) {
        $FK_cityID = $PK_ID;
    } else {
        //inserting data into city table
        $data = array();
        $data['name'] = $addressData['city'];
        $data['FK_stateID'] = $FK_stateID;
        $FK_cityID = insertIntoDB('city', $data, $con);
    }

    //inserting data into address table
    $data = array();
    $data['street'] = $addressData['street'];
    $data['FK_cityID'] = $FK_cityID;
    $data['zipCode'] = $addressData['zipCode'];
    $data['type'] = $addressType;
    $data['FK_employeeID'] = $addressData['FK_employeeID'];
    insertIntoDB('address', $data, $con);
}

//method to insert communication medium and employee communication
function insertCommunicationIntoDB($communicationMedium, $FK_employeeID, $con)
{
    foreach ($communicationMedium as $value) {
        //if communication medium exists
        if ($PK_ID = ifExistsInDB('communication', 'medium', $value, $con)) {
            $FK_communicationID = $PK_ID;
        } else {
            //inserting data into communication table
            $data = array();
            $data['medium'] = $value;
            $FK_communicationID = insertIntoDB('communication', $data, $con);
        }
        //inserting data into employeeCommunication table
        $data = array();
        $data['FK_communicationID'] = $FK_communicationID;
        $data['FK_employeeID'] = $FK_employeeID;
        insertIntoDB('employeeCommunication', $data, $con);
    }
}

//print old field values
function oldValue($index)
{
    if (isset($_SESSION['oldValues'][$index])) {
        return 'value="' . $_SESSION['oldValues'][$index] . '"';
    }
}

//print errors
function ifError($index)
{
    if (isset($_SESSION['errors'][$index])) {
        echo '<span class="text-danger">' . $_SESSION['errors'][$index] . '</span>';
    }
}

//put errors into log
function logError($error)
{
    $now = new DateTime();
    $dateTime = $now->format('[D M d H:i:s Y]');
    $text = "$dateTime\t$error\n";
    $logFile = fopen(CONFIG_PATH . 'error.log', 'a');
    fwrite($logFile, $text);
    fclose($logFile);
}
