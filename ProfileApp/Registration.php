<?php
session_start();
if (isset($_SESSION['loginStatus']) &&  1 == $_SESSION['loginStatus']) {
    header("location:Dashboard.php");
}
require_once('Methods.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Registration</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
        rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Enter Details</h3>
                            </div>
                            <div class="panel-body">
                                <form method="POST" action="Register.php" enctype="multipart/form-data"
                                autocomplete="on" id="regForm">
                                    <div class="form-group">
                                        <label class="control-label" for="prefix">Prefix</label>
                                        <span class="text-danger">*</span>
                                        <select name="prefix" type="select" class="form-control"
                                        id="prefix">
                                        
                                            <option value="Mr."
                                            <?php
                                            if (isset($_SESSION['oldValues']['prefix'])
                                                && $_SESSION['oldValues']['prefix'] == 'Mr.') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Mr.</option>
                                            <option value="Mrs."
                                            <?php
                                            if (isset($_SESSION['oldValues']['prefix'])
                                                && $_SESSION['oldValues']['prefix'] == 'Mrs.') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Mrs.</option>
                                            <option value="Miss."
                                            <?php
                                            if (isset($_SESSION['oldValues']['prefix'])
                                                && $_SESSION['oldValues']['prefix'] == 'Miss.') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Miss.</option>
                                        </select>
                                        <?php echo ifError('prefix'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="firstName">First Name</label>
                                        <span class="text-danger">*</span>
                                        <input name="firstName" type="text" required class="form-control onlyLetter"
                                        <?php echo oldValue('firstName'); ?>
                                        id="firstName" placeholder="David">
                                        <?php echo ifError('firstName'); ?>
                                        <span id="firstNameErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="middleName">Middle Name</label>
                                        <input name="middleName" type="text" class="form-control"
                                        <?php echo oldValue('middleName'); ?>
                                        id="middleName" placeholder="Jhon">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="lastName">Last Name</label>
                                        <span class="text-danger">*</span>
                                        <input name="lastName" type="text" required class="form-control onlyLetter"
                                        <?php echo oldValue('lastName'); ?>
                                        id="lastName" placeholder="Malan">
                                        <?php echo ifError('lastName'); ?>
                                        <span id="lastNameErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="email">Email ID</label>
                                        <span class="text-danger">*</span>
                                        <input name="email" type="email" required class="form-control"
                                        <?php echo oldValue('email'); ?>
                                        id="email" placeholder="example@example.com">
                                        <?php echo ifError('email'); ?>
                                        <span id="emailErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="password">Password</label>
                                        <span class="text-danger">*</span>
                                        <input name="password" type="password" required class="form-control"
                                        id="password" placeholder="****">
                                        <?php echo ifError('password'); ?>
                                        <span id="passwordErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="confirmPassword">
                                            Confirm Password
                                        </label>
                                        <span class="text-danger">*</span>
                                        <input name="confirmPassword" type="password" required
                                        class="form-control" id="confirmPassword" placeholder="****">
                                        <?php echo ifError('confirmPassword'); ?>
                                        <span id="confirmPasswordErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="gender">Gender</label>
                                        <span class="text-danger">*</span>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label>
                                                    <input name="gender" type="radio" value="M"
                                                    <?php
                                                    if (isset($_SESSION['oldValues']['gender'])
                                                        && $_SESSION['oldValues']['gender'] == 'M') {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                    id="genderMale">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label>
                                                    <input name="gender" type="radio" value="F"
                                                    <?php
                                                    if (isset($_SESSION['oldValues']['gender'])
                                                        && $_SESSION['oldValues']['gender'] == 'F') {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                    id="genderFemale">
                                                    Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>                
                                    <?php echo ifError('gender'); ?>
                                    <span id="genderErr" class="text-danger"></span>                    
                                    <span style="margin-top: -1em; display: block;">&nbsp</span>
                                    <div class="form-group">
                                        <label class="control-label" for="dateOfBirth">Date Of Birth</label>
                                        <span class="text-danger">*</span>
                                        <input name="dateOfBirth" type="date" required
                                        <?php echo oldValue('dateOfBirth'); ?>
                                        class="form-control" id="dateOfBirth" placeholder="yyyy-mm-dd">
                                        <?php echo ifError('dateOfBirth'); ?>
                                        <span id="dateOfBirthErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="maritalStatus">
                                            Marital Status
                                        </label>
                                        <select name="maritalStatus" type="select" class="form-control"
                                        id="maritalStatus">
                                            <option value="Married"
                                            <?php
                                            if (isset($_SESSION['oldValues']['maritalStatus'])
                                                && $_SESSION['oldValues']['maritalStatus'] == 'Married') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Married</option>
                                            <option value="Single"
                                            <?php
                                            if (isset($_SESSION['oldValues']['maritalStatus'])
                                                && $_SESSION['oldValues']['maritalStatus'] == 'Single') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Single</option>
                                            <option value="Divorced"
                                            <?php
                                            if (isset($_SESSION['oldValues']['maritalStatus'])
                                                && $_SESSION['oldValues']['maritalStatus'] == 'Divorced') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Divorced</option>
                                            <option value="Widowed"
                                            <?php
                                            if (isset($_SESSION['oldValues']['maritalStatus'])
                                                && $_SESSION['oldValues']['maritalStatus'] == 'Widowed') {
                                                echo 'selected';
                                            }
                                            ?>
                                            >Widowed</option>
                                        </select>
                                        <?php echo ifError('maritalStatus'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="company">Company</label>
                                        <span class="text-danger">*</span>
                                        <input name="company" type="text" required class="form-control"
                                        <?php echo oldValue('company'); ?>
                                        id="company" placeholder="Mindfire">
                                        <?php echo ifError('company'); ?>
                                        <span id="companyErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="employment">Designation</label>
                                        <span class="text-danger">*</span>
                                        <input name="employment" type="text" required class="form-control"
                                        <?php echo oldValue('employment'); ?>
                                        id="employment" placeholder="Software Engineer">
                                        <?php echo ifError('employment'); ?>
                                        <span id="employmentErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="photo">Profile Photo</label>
                                        <input name="photo" type="file" class="form-control" id="photo">
                                        <?php echo ifError('photo'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="residenceAddress">
                                            Residential Address
                                        </label>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label class="control-label" for="residenceStreet">
                                                    Street
                                                </label>
                                                <input name="residenceStreet" type="text"
                                                class="form-control" id="residenceStreet"
                                                <?php echo oldValue('residenceStreet'); ?>
                                                placeholder="No. 6/50G, Shanti Path">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label" for="residenceCity">City</label>
                                                <input name="residenceCity" type="text" class="form-control"
                                                <?php echo oldValue('residenceCity'); ?>
                                                id="residenceCity" placeholder="Chanakyapuri">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label class="control-label" for="residenceState">
                                                    State
                                                </label>
                                                <input name="residenceState" type="text" class="form-control"
                                                <?php echo oldValue('residenceState'); ?>
                                                id="residenceState" placeholder="New Delhi">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label" for="residenceZipCode">
                                                    Zip Code
                                                </label>
                                                <input name="residenceZipCode" max="6" type="text"
                                                class="form-control onlyNumber" id="residenceZipCode"
                                                <?php echo oldValue('residenceZipCode'); ?>
                                                placeholder="110021">
                                            </div>
                                            <span class="text-danger" style="margin-left: 53%">
                                                <?php echo ifError('residenceZipCode'); ?>
                                                <span id="residenceZipCodeErr"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="officeAddress">
                                            Office Address
                                        </label>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label class="control-label" for="officeStreet">
                                                    Street
                                                </label>
                                                <input name="officeStreet" type="text" class="form-control"
                                                <?php echo oldValue('officeStreet'); ?>
                                                id="officeStreet" placeholder="10th Floor, DLF Cyber City">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label" for="officeCity">City</label>
                                                <input name="officeCity" type="text" class="form-control"
                                                <?php echo oldValue('officeCity'); ?>
                                                id="officeCity" placeholder="Bhubaneswar">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label class="control-label" for="officeState">State</label>
                                                <input name="officeState" type="text" class="form-control"
                                                <?php echo oldValue('officeState'); ?>
                                                id="officeState" placeholder="Odisha">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label" for="officeZipCode">
                                                    Zip Code
                                                </label>
                                                <input name="officeZipCode" type="text" class="form-control onlyNumber"
                                                <?php echo oldValue('officeZipCode'); ?>
                                                id="officeZipCode" max="6" placeholder="751024">
                                            </div>
                                            <span class="text-danger" style="margin-left: 53%">
                                                <?php echo ifError('officeZipCode'); ?>
                                                <span id="officeZipCodeErr"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">                                    
                                        <label class="control-label" for="homePhone">
                                            Home Phone Number
                                        </label>
                                        <input name="homePhone" type="text" class="form-control onlyNumber"
                                        <?php echo oldValue('homePhone'); ?>
                                        id="homePhone" max="10" placeholder="3013571463">
                                        <?php echo ifError('homePhone'); ?>
                                        <span id="homePhoneErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="cellPhone">
                                            Cell Phone Number
                                        </label>
                                        <input name="cellPhone" type="text" class="form-control onlyNumber"
                                        <?php echo oldValue('cellPhone'); ?>
                                        id="cellPhone" max="10" placeholder="9883992739">
                                        <?php echo ifError('cellPhone'); ?>
                                        <span id="cellPhoneErr" class="text-danger"></span>
                                    </div>
                                    <?php echo ifError('phoneNumber'); ?>
                                    <span id="phoneNumberErr" class="text-danger"></span>
                                    <div class="form-group">
                                        <label class="control-label" for="fax">Fax Number</label>
                                        <input name="fax" type="text" class="form-control onlyNumber"
                                        <?php echo oldValue('fax'); ?>
                                        id="fax" max="10" placeholder="0704123456">
                                        <?php echo ifError('fax'); ?>
                                        <span id="faxErr" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="communicationMedium">
                                            Communication Medium
                                        </label>
                                        <span class="text-danger">*</span>
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <label for="mail">
                                                    <input name="communicationMedium[]" type="checkbox"
                                                    <?php
                                                    if (isset($_SESSION['oldValues']['communicationMedium'])
                                                        && in_array(
                                                            'mail',
                                                            $_SESSION['oldValues']['communicationMedium']
                                                        )) {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                    value="mail" id="mail">
                                                    Mail
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="message">
                                                    <input name="communicationMedium[]" type="checkbox"
                                                    <?php
                                                    if (isset($_SESSION['oldValues']['communicationMedium'])
                                                        && in_array(
                                                            'message',
                                                            $_SESSION['oldValues']['communicationMedium']
                                                        )) {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                    value="message" id="message">
                                                    Message
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="call">
                                                    <input name="communicationMedium[]" type="checkbox"
                                                    <?php
                                                    if (isset($_SESSION['oldValues']['communicationMedium'])
                                                        && in_array(
                                                            'call',
                                                            $_SESSION['oldValues']['communicationMedium']
                                                        )) {
                                                        echo 'checked';
                                                    }
                                                    ?>
                                                    value="call"  id="call">
                                                    Call
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo ifError('communicationMedium'); ?>
                                    <span id="communicationMediumErr" class="text-danger"></span>
                                    <span style="margin-top: -1em; display: block;">&nbsp</span>
                                    <div class="form-group">
                                        <label class="control-label" for="extraNote">Extra Note</label>
                                        <textarea name="extraNote" class="form-control" id="extraNote">
                                            <?php
                                            if (isset($_SESSION['oldValues']['extraNote'])) {
                                                echo $_SESSION['oldValues']['extraNote'];
                                            }
                                            ?>
                                        </textarea>
                                    </div>
                                    <input type="submit" class="btn btn-default" name="submit"
                                    id="submit" value="Register">
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </div>
        </div>
        <script type="text/javascript"
        src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js">
        </script>
        <script type="text/javascript"
        src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js">
        </script>
        <script type="text/javascript"src="Script.js">    
        </script>
    </body>
</html>
<?php
unset($_SESSION['errors']);
unset($_SESSION['oldValues']);
?>